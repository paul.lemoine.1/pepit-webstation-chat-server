var gulp = require("gulp");
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");

gulp.task("build", function () {
    return tsProject.src()
        .pipe(tsProject())
        .on("error", function (err) {
            console.log("Error :" + err);
        })
        .js.pipe(gulp.dest("./dist"));
});