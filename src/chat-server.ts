import { createServer, Server } from "http";
import * as express from "express";
import * as socketIo from "socket.io";

import { Message } from "./model/message";
import { User } from "./model";

export class ChatServer {
  public static readonly PORT: number = 9090;
  private app: express.Application;
  private server: Server;
  private io: SocketIO.Server;
  private port: string | number;
  private userServ: User;
  private listUser: User[] = [];
  private rooms: Map<string, string>;
  private roomName: string;
  private numberRoom: number;

  constructor() {
    this.init();
    this.createApp();
    this.config();
    this.createServer();
    this.sockets();
    this.listen();
  }

  private init() {
    this.numberRoom = 0;
    this.roomName = "room_";
    this.rooms = new Map<string, string>();
    this.userServ = new User("Serveur", null);
  }

  private createApp(): void {
    this.app = express();
    this.app.use(function (req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
    });
  }

  private createServer(): void {
    this.server = createServer(this.app);
  }

  private config(): void {
    this.port = process.env.PORT;
  }

  private sockets(): void {
    this.io = socketIo(this.server);
  }

  private listen(): void {
    this.server.listen(this.port, () => {
      console.log("Running server on port %s", this.port);
    });

    this.io.on("connect", (socket: any) => {
      console.log("Connected client on port %s.", this.port);

      this.awaitingRoom(socket);
      this.numberRoom++;
    });
  }
  public awaitingRoom(socket: any) {
    socket.on("AwaitingUser", (user: any) => {
      switch (user.userRoleDto.authorityDtoName) {
        case "USER": {
          console.log("Je suis un user.");
          this.createRoom(socket);
          break;
        }
        case "ADMIN": {
          console.log("Je suis un modérateur.");
          this.findRoom(socket);
          socket.on("refresh", () => {
            this.findRoom(socket);
          });
        }
      }

      /* 
      this.createRoom(socket);
      socket.join(this.rooms[this.numberRoom], () => {
        this.rooms = Object.keys(socket.rooms);
        console.log(this.rooms); // [ <socket.id>, 'room 237' ]
      });

      console.log(this.rooms[this.numberRoom]);
      const messCo = new Message(
        this.userServ,
        'Vous êtes connecté au serveur',
        this.rooms[this.numberRoom]
      );
      
      this.io.to(this.rooms[this.numberRoom]).emit('message', messCo);*/

      /*this.io.emit('message', messCo);*/
    });
  }

  public createRoom(socket: any) {
    let roomName = this.roomName + this.numberRoom;
    socket.join(roomName, () => {
      this.rooms.set(socket.id, roomName);
      this.onMessage(socket);
      this.numberRoom++;
    });
  }

  public onMessage(socket: any) {
    let idRoom = this.rooms.get(socket.id);
    console.log("Id de la room pour un user: " + idRoom);

    socket.on("forceDisconnect", () => {
      console.log("Client disconnected");
      const messCoAdmin = new Message(
        this.userServ,
        "Vous avez été déconnecté.",
        idRoom
      );
      this.io.to(idRoom).emit("message", messCoAdmin);
      socket.disconnect();
    });

    socket.on("message", (m: Message) => {
      this.io.to(idRoom).emit("message", m);
      console.log("Client dsqdd");
    });


    const messCo = new Message(
      this.userServ,
      "En attente d'un modérateur.",
      idRoom
    );
    this.io.to(idRoom).emit("getRoom", messCo);
  }

  public findRoom(socket: any) {
    if (this.rooms.size > 0) {
      let idRoom = this.rooms.values().next().value;
      console.log("Id de la room pour un modérateur: " + idRoom);
      socket.join(idRoom, () => {
        socket.on("message", (m: Message) => {
          //console.log('[server](message): %s', JSON.stringify(m));
          this.io.to(idRoom).emit("message", m);
        });

        socket.on("forceDisconnect", () => {
          console.log("Client disconnected");
          const messCoAdmin = new Message(
            this.userServ,
            "Vous avez été déconnecté.",
            idRoom
          );
          this.io.to(idRoom).emit("message", messCoAdmin);
          socket.disconnect();
        });

        const messCoAdmin = new Message(
          this.userServ,
          "Un modérateur vient d'arriver.",
          idRoom
        );
        this.io.to(idRoom).emit("message", messCoAdmin);
        let socketKey = this.getSocketId(this.rooms, idRoom);
        console.log(this.rooms.delete(socketKey));
        console.log(this.rooms);
      });
    } else {
      const messCo = new Message(
        this.userServ,
        "Pas d'instances de chat existantes.",
        ""
      );
      this.io.emit("getRoom", messCo);
    }
  }

  getSocketId(rooms: Map<string, string>, idRoom: string): string {
    let socketId: string;
    socketId = "";
    rooms.forEach((value: string, key: string) => {
      console.log(key, value);
      if (idRoom === value) {
        console.log("J'ai trouvé la value.");
        socketId = key;
      }
    });
    return socketId;
  }

  public getApp(): express.Application {
    return this.app;
  }
}
