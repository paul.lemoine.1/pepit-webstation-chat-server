import { User } from './user';
import { Message } from './Message';

export class ChatMessage extends Message {
  constructor(from: User, content: string, room: string) {
    super(from, content, room);
  }
}
