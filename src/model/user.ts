import { Authority } from './authority';

export class User {
  constructor(private firstName: string, private userRoleDto: Authority) {}

  get FirstName(): string {
    return this.firstName;
  }

  get UserRoleDto(): Authority {
    return this.userRoleDto;
  }
}
