"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Message = /** @class */ (function () {
    function Message(from, content, roomName) {
        this.from = from;
        this.content = content;
        this.roomName = roomName;
    }
    return Message;
}());
exports.Message = Message;
