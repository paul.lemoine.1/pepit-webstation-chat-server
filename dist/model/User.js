"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var User = /** @class */ (function () {
    function User(firstName, userRoleDto) {
        this.firstName = firstName;
        this.userRoleDto = userRoleDto;
    }
    Object.defineProperty(User.prototype, "FirstName", {
        get: function () {
            return this.firstName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "UserRoleDto", {
        get: function () {
            return this.userRoleDto;
        },
        enumerable: true,
        configurable: true
    });
    return User;
}());
exports.User = User;
