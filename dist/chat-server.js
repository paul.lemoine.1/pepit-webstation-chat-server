"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("http");
var express = require("express");
var socketIo = require("socket.io");
var message_1 = require("./model/message");
var model_1 = require("./model");
var ChatServer = /** @class */ (function () {
    function ChatServer() {
        this.listUser = [];
        this.init();
        this.createApp();
        this.config();
        this.createServer();
        this.sockets();
        this.listen();
    }
    ChatServer.prototype.init = function () {
        this.numberRoom = 0;
        this.roomName = "room_";
        this.rooms = new Map();
        this.userServ = new model_1.User("Serveur", null);
    };
    ChatServer.prototype.createApp = function () {
        this.app = express();
        this.app.use(function (req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
        });
    };
    ChatServer.prototype.createServer = function () {
        this.server = http_1.createServer(this.app);
    };
    ChatServer.prototype.config = function () {
        this.port = process.env.PORT;
    };
    ChatServer.prototype.sockets = function () {
        this.io = socketIo(this.server);
    };
    ChatServer.prototype.listen = function () {
        var _this = this;
        this.server.listen(this.port, function () {
            console.log("Running server on port %s", _this.port);
        });
        this.io.on("connect", function (socket) {
            console.log("Connected client on port %s.", _this.port);
            _this.awaitingRoom(socket);
            _this.numberRoom++;
        });
    };
    ChatServer.prototype.awaitingRoom = function (socket) {
        var _this = this;
        socket.on("AwaitingUser", function (user) {
            switch (user.userRoleDto.authorityDtoName) {
                case "USER": {
                    console.log("Je suis un user.");
                    _this.createRoom(socket);
                    break;
                }
                case "ADMIN": {
                    console.log("Je suis un modérateur.");
                    _this.findRoom(socket);
                    socket.on("refresh", function () {
                        _this.findRoom(socket);
                    });
                }
            }
            /*
            this.createRoom(socket);
            socket.join(this.rooms[this.numberRoom], () => {
              this.rooms = Object.keys(socket.rooms);
              console.log(this.rooms); // [ <socket.id>, 'room 237' ]
            });
      
            console.log(this.rooms[this.numberRoom]);
            const messCo = new Message(
              this.userServ,
              'Vous êtes connecté au serveur',
              this.rooms[this.numberRoom]
            );
            
            this.io.to(this.rooms[this.numberRoom]).emit('message', messCo);*/
            /*this.io.emit('message', messCo);*/
        });
    };
    ChatServer.prototype.createRoom = function (socket) {
        var _this = this;
        var roomName = this.roomName + this.numberRoom;
        socket.join(roomName, function () {
            _this.rooms.set(socket.id, roomName);
            _this.onMessage(socket);
            _this.numberRoom++;
        });
    };
    ChatServer.prototype.onMessage = function (socket) {
        var _this = this;
        var idRoom = this.rooms.get(socket.id);
        console.log("Id de la room pour un user: " + idRoom);
        socket.on("forceDisconnect", function () {
            console.log("Client disconnected");
            var messCoAdmin = new message_1.Message(_this.userServ, "Vous avez été déconnecté.", idRoom);
            _this.io.to(idRoom).emit("message", messCoAdmin);
            socket.disconnect();
        });
        socket.on("message", function (m) {
            _this.io.to(idRoom).emit("message", m);
            console.log("Client dsqdd");
        });
        var messCo = new message_1.Message(this.userServ, "En attente d'un modérateur.", idRoom);
        this.io.to(idRoom).emit("getRoom", messCo);
    };
    ChatServer.prototype.findRoom = function (socket) {
        var _this = this;
        if (this.rooms.size > 0) {
            var idRoom_1 = this.rooms.values().next().value;
            console.log("Id de la room pour un modérateur: " + idRoom_1);
            socket.join(idRoom_1, function () {
                socket.on("message", function (m) {
                    //console.log('[server](message): %s', JSON.stringify(m));
                    _this.io.to(idRoom_1).emit("message", m);
                });
                socket.on("forceDisconnect", function () {
                    console.log("Client disconnected");
                    var messCoAdmin = new message_1.Message(_this.userServ, "Vous avez été déconnecté.", idRoom_1);
                    _this.io.to(idRoom_1).emit("message", messCoAdmin);
                    socket.disconnect();
                });
                var messCoAdmin = new message_1.Message(_this.userServ, "Un modérateur vient d'arriver.", idRoom_1);
                _this.io.to(idRoom_1).emit("message", messCoAdmin);
                var socketKey = _this.getSocketId(_this.rooms, idRoom_1);
                console.log(_this.rooms.delete(socketKey));
                console.log(_this.rooms);
            });
        }
        else {
            var messCo = new message_1.Message(this.userServ, "Pas d'instances de chat existantes.", "");
            this.io.emit("getRoom", messCo);
        }
    };
    ChatServer.prototype.getSocketId = function (rooms, idRoom) {
        var socketId;
        socketId = "";
        rooms.forEach(function (value, key) {
            console.log(key, value);
            if (idRoom === value) {
                console.log("J'ai trouvé la value.");
                socketId = key;
            }
        });
        return socketId;
    };
    ChatServer.prototype.getApp = function () {
        return this.app;
    };
    ChatServer.PORT = 9090;
    return ChatServer;
}());
exports.ChatServer = ChatServer;
